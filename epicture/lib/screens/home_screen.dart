import 'package:flutter/material.dart';
import 'package:epicture/network/imgur_requests.dart';

///this class is called HomeScreen
///this class will allow use to switch with the other screen
///in addition HomeScreen display images of imgur

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  static var myController = new TextEditingController(text: "rat");
  static TextStyle textStyle =
      TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    myController = new TextEditingController(text: "rat");
    super.dispose();
  }

  final searchField = TextField(
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "Search",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0))),
    onChanged: (text) {
      myController.text = text;
    },
  );

  @override
  Widget build(BuildContext context) {
    void changeScreen(context, String route) {
      Navigator.of(context).pushNamed(route);
    }

    getPics(String search) async {
      print(search);
      if (search.isEmpty == false) {
        print(search);
        var map = await ImgurRequests.search(search, 0);
        return map;
      } else {
        var map = await ImgurRequests.search("dog", 1);
        return map;
      }
    }

    final makeBottom = Container(
      height: 55.0,
      child: BottomAppBar(
        color: Colors.blue,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.account_box, color: Colors.white),
              onPressed: () {
                changeScreen(context, "/myaccount");
              },
            ),
            IconButton(
              icon: Icon(Icons.add_a_photo, color: Colors.white),
              onPressed: () {
                changeScreen(context, "/myuploader");
              },
            ),
            IconButton(
              icon: Icon(Icons.favorite, color: Colors.white),
              onPressed: () {
                changeScreen(context, "/myfavorite");
              },
            ),
            IconButton(
              icon: Icon(Icons.settings, color: Colors.white),
              onPressed: () {
                changeScreen(context, "/settings");
              },
            )
          ],
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start, //center
            children: <Widget>[
              searchField,
              new FutureBuilder(
                future: getPics(myController.text),
                builder: (context, snapShot) {
                  var data = snapShot.data;
                  if (snapShot.hasError) {
                    print("-----------------------ERROR-------------------");
                    print(snapShot.error);
                    return Text(
                      "Failed to get response from the server",
                      style: TextStyle(color: Colors.red, fontSize: 22.0),
                    );
                  } else if (snapShot.hasData) {
                    return Center(
                      child: SizedBox(
                          height: 350,
                          child: new ListView.builder(
                            itemCount: data.length,
                            itemBuilder: (context, index) {
                              // print("INDEX = ");
                              // print(index);
                              return new Column(
                                children: <Widget>[
                                  new Padding(
                                      padding: const EdgeInsets.all(5.0)),
                                  new Container(
                                    child: new InkWell(
                                      onTap: () {},
                                      child: new Image.network(
                                          '${data[index]['link']}'),
                                    ),
                                  ),
                                ],
                              );
                            },
                          )),
                    );
                  } else if (!snapShot.hasData) {
                    print(
                        "=========================IAM LOADING===============================");
                    return new Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ],
          )),
      bottomNavigationBar: makeBottom,
    );
  }
}