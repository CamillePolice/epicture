import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../network/imgur_requests.dart';

///This class is called myPhotos
///the aim of this class is to upload photos to our imgur account
class MyUploader extends StatefulWidget {
  @override
  _MyUploaderState createState() => _MyUploaderState();
}

class _MyUploaderState extends State<MyUploader> {
  static File _image;
  static TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }
  static var myController = new TextEditingController(text: "Default Title");
  static var myDescriptionController = new TextEditingController(text: "Default description");

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    myDescriptionController.dispose();
    myController = new TextEditingController(text: "Default Title");
    myDescriptionController = new TextEditingController(text: "Default description");
    super.dispose();
  }
  final titleField = TextField(
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "write a Title",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0))),
    onChanged: (text) {
      myController.text = text;
    },
  );

    final descriptionField = TextField(
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        hintText: "write a short description",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(40.0))),
    onChanged: (text) {
      myDescriptionController.text = text;
    },
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Image Picker Example'),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    titleField,
                    descriptionField,
                    SizedBox(
                      height: 400.0,
                      child: _image == null
                          ? Text('No image selected.')
                          : Image.file(_image),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(30.0),
                      color: Color(0xff01A0C7),
                      child: MaterialButton(
                        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        onPressed: getImage,
                        child: Text("Pick an Image",
                            textAlign: TextAlign.center,
                            style: style.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(30.0),
                      color: Color(0xff01A0C7),
                      child: MaterialButton(
                        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        onPressed: () async {
                          if (_image != null) {
                            var response = await ImgurRequests.uploadImage(
                                myController.text, myDescriptionController.text, _image, "0");
                            print(response.toString());
                            if (response != null && response == 200) {
                              AlertDialog(
                                title: Text('Request success'),
                                content: const Text('Request success'),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            } else {
                              AlertDialog(
                                title: Text('Request failure'),
                                content: const Text('Request failure'),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            }
                          }
                        },
                        child: Text("Upload on imgur",
                            textAlign: TextAlign.center,
                            style: style.copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}

/*
return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
      ),
      body: Center(
        child: _image == null ? Text('No image selected.') : Image.file(_image),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
*/
