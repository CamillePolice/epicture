import 'package:epicture/network/imgur_requests.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


///This class is called MyAccount
///it is a widget that will display all info of the current user
///its will also search other users Note: this feature is note implemented
class MyAccount extends StatefulWidget {
  MyAccount({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  //final username = globals.username;
  getMyAccount() async {
      var map = await ImgurRequests.getAccount("me");
      return map;
  }

  getMyAccountImages() async {
    var map = await ImgurRequests.getAccountImage();
    return map;
  }

  @override
  Widget build(BuildContext context) {
    //print("+++++++++++++++++++++++++++++++++++++++++");
     //print(username);
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Username",
            ),
            new FutureBuilder (
              future: getMyAccount(),
              builder: (context, snapShot) {
                var data = snapShot.data;
                if (snapShot.hasError) {
                  print("-----------------------ERROR-------------------");
                  print(snapShot.error);
                  return Text("Failed to get response from the server", style: TextStyle(color: Colors.red, fontSize: 22.0),
                  );
                }
                else if (snapShot.hasData) {
                  return Column (
                    children: <Widget>[
                      Text( '${data['url']}', style: TextStyle(color: Colors.black87, fontSize: 22.0)),
                      Image.network('${data['avatar']}'),
                      //Image.network(
                       // '${data['cover']}',
                        //width: size.width,
                        //height: size.height,
                        //fit: BoxFit.fill,
                      //),
                    ],
                    );
                }
                else if (!snapShot.hasData) {
                  print("=========================IAM LOADING===============================");
                  return new Center(child: CircularProgressIndicator(),);
                }
              },
            ),

            new FutureBuilder (
              future: getMyAccountImages(),
              builder: (context, snapShot) {
                var data = snapShot.data;
                print(data);
                if (snapShot.hasError) {
                  print("-----------------------ERROR-------------------");
                  print(snapShot.error);
                  return Text("Failed to get response from the server", style: TextStyle(color: Colors.red, fontSize: 22.0),
                  );
                }
                else if (snapShot.hasData) {
                    return Center(
                      child: SizedBox(
                          height: 350,
                          child: new ListView.builder(
                            itemCount: data.length,
                            itemBuilder: (context, index) {
                              return new Column(
                                children: <Widget>[
                                  new Padding(
                                      padding: const EdgeInsets.all(10.0)),
                                  new Container(
                                    child: new InkWell(
                                      onTap: () {
                                        Alert(context: context, title: "Title : ${data[index]['title']}", desc: "Desc : ${data[index]['description']}").show();
                                      },
                                      child: new Image.network(
                                          '${data[index]['link']}'),
                                    ),
                                  ),
                                ],
                              );
                            },
                          )),
                    );
                }
                else if (!snapShot.hasData) {
                  print("=========================IAM LOADING===============================");
                  return new Center(child: CircularProgressIndicator(),);
                }
              },
            ),


          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search), 
        onPressed: () {},
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}