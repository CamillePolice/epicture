import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(
      url:
          "https://api.imgur.com/oauth2/authorize?client_id=${DotEnv().env['IMGUR_CLIENT_ID']}&response_type=token",
      appBar: new AppBar(
        title: new Text("Widget webview"),
      ),
    );
  }
}
