import 'dart:convert';
import 'dart:io';
import 'package:epicture/models/user_datas.dart' as globals;
import 'package:http/http.dart' as http;

/// the name of this class is ImgurRequests.
/// The aim of this class is to encapsulate all api requests
/// ImgurRequests will simplify the usage of the api
class ImgurRequests {
  static const globalEndpoint = "https://api.imgur.com/3/";

/// this function will get the headers of each request that we will send
  static getHeaders() {
    Map<String, String> header = {};
    if (globals.accessToken != null) {
      header["Authorization"] = "Bearer " + globals.accessToken;
    } else {
      header["Authorization"] = "Client-ID " + globals.clientId;
    }

    return header;
  }

/// this function will send request to the api in order to get images
  static getImages({String type = "hot", int page = 0}) async {
    // flutter_test
    var uri = globalEndpoint +
        "gallery/" +
        type +
        "/time/" +
        page.toString() +
        "?showViral=true&mature=true&album_previews=true";
    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get albumImages
  static getAlbumImages(String galleryHash) async {
    // flutter_test
    var uri = globalEndpoint + "album/$galleryHash/images";
    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to search an image
  static search(String search, int page) async {
    // flutter_test
    var uri = globalEndpoint +
        "gallery/search/time/all/" +
        page.toString() +
        "?q=" +
        search;
    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get tags
  static getTags() async {
    // flutter_test
    var uri = globalEndpoint + "tags";

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());
    var ret = json.decode(res.body)["data"]["tags"];

    for (var r in ret) {
      r["background"] =
          "https://i.ImgurRequests.com/" + r["background_hash"] + ".jpg";
    }
    return ret;
  }

/// this function will send request to the api in order to get tag dearch
  static tagSearch({String tag = "", int page = 0}) async {
    // flutter_test
    var uri =
        globalEndpoint + "gallery/t/" + tag + "/time/all/" + page.toString();

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get comments
  static getComments(galleryHash, {sort: "best"}) async {
    var uri = globalEndpoint + "gallery/" + galleryHash + "/comments/" + sort;

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }
/// this function will send request to the api in order to get avatarAccount
  static getAvatarAccount(username) async {
    // flutter_test
    var uri = globalEndpoint + "account/" + username + "/avatar";

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());
    if (res.body == "") return {"avatar": ""};

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get an Account
  static getAccount(username) async {
    // flutter_test
    var uri = globalEndpoint + "account/" + username;

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    var data = json.decode(res.body)["data"];
    data["createdAt"] =
        new DateTime.fromMillisecondsSinceEpoch(data["created"] * 1000);
    return data;
  }

/// this function will send request to the api in order to get images of user
  static getImagesOfUser({page = 0, username}) async {
    var uri = globalEndpoint +
        "account/" +
        username +
        "/submissions/" +
        page.toString();

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to delete user images
  static destroyImageOfUser(username, id) async {
    var uri = globalEndpoint + "/image/$id";

    var res = await http.delete(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to share image with the community
  static shareImageWithTheCommunity(id, title, topic, tags) async {
    var uri = globalEndpoint + "/gallery/image/$id";

    var res = await http
        .post(Uri.encodeFull(uri), headers: ImgurRequests.getHeaders(), body: {
      "title": title,
      "topic": topic,
      "terms": "1",
      "mature": "0",
      "tags": tags,
    });
    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get share album with the community
  static shareAlbumWithTheCommunity(id, title, topic, tags) async {
    var uri = globalEndpoint + "/gallery/album/$id";

    var res = await http
        .post(Uri.encodeFull(uri), headers: ImgurRequests.getHeaders(), body: {
      "title": title,
      "topic": topic,
      "terms": "1",
      "mature": "0",
      "tags": tags,
    });
    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to remove from the gallery
  static removeFromTheGallery(id) async {
    var uri = globalEndpoint + "/gallery/$id";

    var res = await http.delete(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());
    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get tokens
  static getToken() async {
    var uri = "https://api.ImgurRequests.com/oauth2/token";
    var data = {
      "refresh_token": globals.refreshToken,
      "client_id": globals.clientId,
      "client_secret": globals.clientSecret,
      "grant_type": "refresh_token"
    };

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders(), body: data);
    return json.decode(res.body);
  }

/// this function will send request to the api in order to vote
  static vote(String galleryHash, String vote) async {
    assert(vote == "up" || vote == "down" || vote == "veto");

    var uri = globalEndpoint + "gallery/" + galleryHash + "/vote/" + vote;

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return res.statusCode;
  }

/// this function will send request to the api in order to put an image as favorite
  static imageFav(String imageHash) async {
    var uri = globalEndpoint + "image/" + imageHash + "/favorite";

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return res.statusCode;
  }

/// this function will send request to the api in order to get favorite images
  static albumFav(String albumHash) async {
    var uri = globalEndpoint + "album/" + albumHash + "/favorite";

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to commit
  static commentVote(String commentId, String vote) async {
    assert(vote == "up" || vote == "down" || vote == "veto");
    var uri = globalEndpoint + "comment/" + commentId + "/vote/" + vote;

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return res.statusCode;
  }

/// this function will send request to the api in order to follow a tag
  static followTag(String tagName) async {
    var uri = globalEndpoint + "account/me/follow/tag/" + tagName;

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return res.statusCode;
  }

/// this function will send request to the api in order to unfollow a tag
  static unfollowTag(String tagName) async {
    var uri = globalEndpoint + "account/me/follow/tag/" + tagName;

    var res = await http.delete(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return res.statusCode;
  }

/// this function will send request to the api in order to get a tag
  static getTag(String tagName) async {
    var uri = globalEndpoint + "gallery/tag_info/" + tagName;

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get a favorite
  static getFavorites(
      {String username, int page = 0, String sort = "newest"}) async {
    var uri =
        globalEndpoint + "account/$username/gallery_favorites/$page/$sort";

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to send reply
  static sendReply({String imageId, String commentId, String comment}) async {
    var uri = globalEndpoint + "comment/$commentId";
    Map<String, String> data = {"image_id": imageId, "comment": comment};

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders(), body: data);

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get comment
  static getComment({String commentId}) async {
    var uri = globalEndpoint + "comment/$commentId";

    var res = await http.get(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to remove comment
  static delComment({String commentId}) async {
    var uri = globalEndpoint + "comment/$commentId";

    var res = await http.delete(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders());

    return res.statusCode;
  }

/// this function will send request to the api in order to send comment
  static sendComment({imageId, commentId, comment}) async {
    var uri = globalEndpoint;

    if (commentId != null) {
      uri += "comment/$commentId";
    } else {
      uri += "comment";
    }

    var data = {"image_id": imageId, "comment": comment};

    var res = await http.post(Uri.encodeFull(uri),
        headers: ImgurRequests.getHeaders(), body: data);
    return json.decode(res.body)["data"];
  }

/// this function will send request to the api in order to get account Image
  static getAccountImage() async {
    var uri = globalEndpoint + "account/me/images";

    var res = await http.get(uri, headers: ImgurRequests.getHeaders());
    var data = json.decode(res.body)["data"];

    for (var d in data) {
      if (d["ups"] == null) d["ups"] = 0;
      if (d["downs"] == null) d["downs"] = 0;
      if (d["favorite_count"] == null) d["favorite_count"] = 0;
      if (d["score"] == null) d["score"] = 0;
    }
    return data;
  }

/// this function will send request to the api in order to get album from user
  static getAlbumListFromUser(username) async {
    var url = globalEndpoint + "account/$username/albums/";
    var response = await http.get(url, headers: ImgurRequests.getHeaders());
    return await json.decode(response.body);
  }

/// this function will send request to the api in order to get album by page
  static getAlbumListFromUserByPage(page, username) async {
    var url = globalEndpoint + "account/$username/albums/$page";
    var response = await http.get(url, headers: ImgurRequests.getHeaders());
    return await json.decode(response.body);
  }

/// this function will send request to the api in order to get album data
  static getAlbumData(id, username) async {
    var url = globalEndpoint + "account/$username/album/$id";
    var response = await http.get(url, headers: ImgurRequests.getHeaders());
    return await json.decode(response.body)["data"];
  }

/// this function will send request to the api in order to add an image to an album
  static addImageToAnAlbum(id, image) async {
    var url = globalEndpoint + "/album/$id/add";
    var response = await http
        .post(url, headers: ImgurRequests.getHeaders(), body: {"ids[]": image});
    return await json.decode(response.body)["data"];
  }

/// this function will send request to the api in order to remove image from an album
  static removeImageToAnAlbum(id, image) async {
    var url = globalEndpoint + "/album/$id/remove_images";
    var response = await http
        .post(url, headers: ImgurRequests.getHeaders(), body: {"ids[]": image});
    return await json.decode(response.body)["data"];
  }

/// this function will send request to the api in order to create an album
  static createAlbum(title, description, privacy) async {
    var url = globalEndpoint + "album";

    var response = await http.post(url,
        headers: ImgurRequests.getHeaders(),
        body: {"title": title, "description": description, "privacy": privacy});
    return await json.decode(response.body);
  }

/// this function will send request to the api in order to remove an album
  static deleteAlbum(id) async {
    var url = globalEndpoint + "/album/$id";
    var response = await http.delete(url, headers: ImgurRequests.getHeaders());
    return await json.decode(response.body);
  }

/// this function will send request to the api in order to update an album
  static updateAlbumParams(id, title, description, privacy) async {
    var url = globalEndpoint + "album/$id";

    var response = await http.post(url,
        headers: ImgurRequests.getHeaders(),
        body: {"title": title, "description": description, "privacy": privacy});
    return await json.decode(response.body);
  }

/// this function will send request to the api in order to update an image
  static updateImageParams(id, title, description) async {
    var url = globalEndpoint + "image/$id";

    var response =
        await http.post(url, headers: ImgurRequests.getHeaders(), body: {
      "title": title,
      "description": description,
    });
    return await json.decode(response.body);
  }

/// this function will send request to the api in order to get an album image
  static getAlbumImage(id) async {
    var url = globalEndpoint + "/album/$id/images";
    var response = await http.get(url, headers: ImgurRequests.getHeaders());
    return await json.decode(response.body)["data"];
  }

/// this function will send request to the api in order to upload an image on imgur
  static uploadImage(title, description, File image, privacy) async {
    var url = globalEndpoint + "image";

    List<int> imageBytes = image.readAsBytesSync();
    String base64Image = base64Encode(imageBytes);

    var response =
        await http.post(url, headers: ImgurRequests.getHeaders(), body: {
      "title": title,
      "description": description,
      "image": base64Image,
      "privacy": privacy
    });
    return await json.decode(response.body);
  }
}
