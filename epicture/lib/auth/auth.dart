import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:epicture/models/user_datas.dart' as globals;

class Auth extends StatefulWidget {
  Auth({Key key}) : super(key: key);

  @override
  AuthState createState() => new AuthState();
}

class AuthState extends State<Auth> {
  TextEditingController textEditingController;
  FlutterWebviewPlugin flutterWebviewPlugin = new FlutterWebviewPlugin();
  var _listener;
  static final uri = "https://api.imgur.com/oauth2/authorize";
  static final responseUri = "https://imgur.com/#";

  @override
  Widget build(BuildContext context) {
    String uri = AuthState.uri +
        "?client_id=${DotEnv().env['IMGUR_CLIENT_ID']}&response_type=token";
    return WebviewScaffold(
        clearCache: true,
        clearCookies: true,
        appBar: AppBar(title: Text("Auth")),
        url: uri,
        withZoom: false);
  }

  getParams(String url) {
    var response = {};
    var params = url.substring(AuthState.responseUri.length);

    for (var param in params.split("&")) {
      var values = param.split("=");

      response[values[0]] = values[1];
    }
    return response;
  }

  close() async {
    await flutterWebviewPlugin.close();
    Navigator.of(context).pushNamed("/home");
  }

  @override
  void initState() {
    super.initState();

    this._listener = this
        .flutterWebviewPlugin
        .onStateChanged
        .listen((WebViewStateChanged state) async {
      var url = state.url;
      RegExp exp = new RegExp("^$responseUri");

      if (exp.hasMatch(url)) {
        var params = getParams(url);

        setState(() {
          globals.clientId = DotEnv().env['IMGUR_CLIENT_ID'];
          globals.expireIn = int.parse(params["expires_in"]); //Todo  
          globals.accessToken = params["access_token"];
          globals.refreshToken = params["refresh_token"];
          globals.createdAt = new DateTime.now();
          close();
        });
      }
    });
  }

  @override
  void dispose() {
    _listener.cancel();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }
}
