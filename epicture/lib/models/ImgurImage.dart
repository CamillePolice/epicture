import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class User {
  final String url;
  final String avatar;

  User({this.url, this.avatar});

  factory User.fromJson(Map<String, dynamic> json) {
    print(json);
    return User(
      url: json['data']['url'],
      avatar: json['data']['avatar'],
    );
  }
}

Future<User> fetchUser() async {
  final response = await http.get(
    "https://api.imgur.com/oauth2/authorize?"
    "${DotEnv().env['IMGUR_CLIENT_ID']}"
    "&response_id=token",
  );
  if (response.statusCode == 200) {
    return User.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to fetch user');
  }
}
