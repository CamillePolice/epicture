import 'package:epicture/auth/auth.dart';
import 'package:epicture/screens/login.dart';
import 'package:epicture/screens/my_account.dart';
import 'package:epicture/screens/my_favorite.dart';
import 'package:epicture/screens/my_uploader.dart';
import 'package:epicture/screens/settings.dart';
import 'package:flutter/material.dart';
import 'package:epicture/screens/home_screen.dart';

/// routes variable contain all routes of the project
/// each route redirect the user into another widget
  
final routes = {
  '/myaccount': (BuildContext context) => new MyAccount(title: 'My account'),
  '/myuploader': (BuildContext context) => new MyUploader(),
  '/myfavorite': (BuildContext context) => new MyFavorite(title: 'My favorite'),
  '/settings': (BuildContext context) => new Settings(title: 'Settings'),
  '/': (BuildContext context) => new Login(),
  '/webview': (BuildContext context) => new Auth(),
  '/home': (BuildContext context) => new HomeScreen(),
};
