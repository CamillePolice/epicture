import 'package:flutter/material.dart';
import 'package:epicture/routes.dart';
import 'dart:async';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'models/theme_datas.dart';
import 'models/themes.dart';

/// this is the main function
/// the program start here
/// just like C or Cpp languages

Future main() async {
  await DotEnv().load('.env');
  runApp(
    CustomTheme(
      initialThemeKey: MyThemeKeys.LIGHT,
      child: Epicture(),
    ),
  );
}

///main widget
class Epicture extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Epicture',
      theme: CustomTheme.of(context),
      routes: routes,
    );
  }
}
