# Epicture

A new Flutter project.
The goal is to have an application which can makes API calls to Imgur's API
## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

Contributors :
Hadi Bereksi (hadi-ilies.bereksi-reguig@epitech.eu)
Camille Police  (camille.police@epitech.eu)